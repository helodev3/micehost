function updateProgress (progressUrl) {
    fetch(progressUrl).then(function(response) {
        response.json().then(function(data) {
            // update the appropriate UI components
            setProgress(data.taille);
            setTimeout(updateProgress, 500, progressUrl);
        });
    });
}

function setProgress( progress) {

    progressBarElement = document.getElementById("progress")
    progressBarElement = document.getElementById("progress-statu")
    progressBarElement.style.width = progress + "%";
    progressBarMessageElement.innerHTML = progress + ' percent ';
}


var progressUrl = 'localhost:8000/progress/';  // django template usage
updateProgress(progressUrl);