import subprocess
import json
import yaml


def create_template(host, owner):

    dict_value = {
        "http_host": host,
        "http_port": 80,
        "site_owner": owner,
        "http_conf": host+".conf",
        "pass": "microisce"

    }

    try:
        with open("vars/default.yml", "w") as default_yml:
            documents = yaml.dump(dict_value, default_yml)

        return True
    except:
        return False


def prepare_vm():
    process = subprocess.Popen(['ansible-playbook', 'playbook/server_initiate.yml', '-u bitnami'],
                               stdout=subprocess.PIPE,
                               universal_newlines=True)
    while True:
        output = process.stdout.readline()
        print(output.strip())
        #my_string += output.strip()
        # Do something else
        return_code = process.poll()
        if return_code is not None:
            print('RETURN CODE', return_code)
            # Process has finished, read rest of the output
            for output in process.stdout.readlines():
                print(output.strip())
            break


def create_site():

    process = subprocess.Popen(['ansible-playbook', 'vars/default.yml', '-u bitnami'],
                               stdout=subprocess.PIPE,
                               universal_newlines=True)
    my_string = ""
    while True:
        output = process.stdout.readline()
#        #print(output.strip())
        my_string += output.strip()
        # Do something else
        return_code = process.poll()
        if return_code is not None:
            print('RETURN CODE', return_code)
            # Process has finished, read rest of the output
            for output in process.stdout.readlines():
                print(output.strip())
            break
    try:

        value = json.loads(my_string)["stats"]
        return value
    except:

        return None


#prepare_vm()

"""for site in ["consultek"]:
    if create_template(site+".ltd", site+".ltd", site) is not None:
        create_site()
    else:
        print("Error while creating", site)
        
        
        
        
import ansible_runner
r = ansible_runner.run(private_data_dir='./', playbook='deploy_site.yml')
print("{}: {}".format(r.status, r.rc))
# successful: 0
for each_host_event in r.events:
    print(each_host_event['event'])
print("Final status:")
print(r.stats)        
        
        
        
"""

