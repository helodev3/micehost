import yaml
from celery import shared_task
from celery.utils.log import get_task_logger
from time import sleep
import ansible_runner
import zipfile
import os
#TODO (DEV) handle runner_on_failed and/or rescue block for each event
logger = get_task_logger(__name__)


@shared_task(name='freedeployment')
def free_deployment(username, domain, url, worker_id):
    zip_folder = "/Users/mac/cloud/microisce/hosting/"+username+"-"+domain
    with zipfile.ZipFile("/Users/mac/cloud/microisce/hosting/ansible.zip", 'r') as zip_ref:
        zip_ref.extractall(zip_folder)

    create_free_template(host=domain, owner=username, folder=zip_folder, url=url, worker_id=worker_id)

    r = ansible_runner.run(private_data_dir=zip_folder+"/ansible",
                           playbook=zip_folder+'/ansible/project/deploy_free.yml')

    return os.getcwd()


def create_free_template(host, owner, folder, url, worker_id):

    shared_vars = {
        "http_host": host,
        "url": url,
        "site_owner": owner
    }

    callback_vars = {
        "runner_http_url": "http://127.0.0.1:8000/updateprogress/"+str(worker_id)+"/",
        "runner_http_headers": {"worker_id": "worker"}
    }
    path = folder+"/ansible/project/roles"
    try:
        with open(path+"/freehosting/vars/main.yml", "w") as share_yml:
            documents = yaml.dump(shared_vars, share_yml)
        with open(folder+"/ansible/env/settings", "w") as settings_yml:
            documents = yaml.dump(callback_vars, settings_yml)
        return
    except:
        return


@shared_task(name='deployment')
def deployment(username, domain, fb_user, fb_pass, product, worker_id):

    if domain.split(".")[0] == "www.":
        domain = domain[4:]
    print(domain)
    zip_folder = "/Users/mac/cloud/microisce/hosting/"+username+"-"+domain
    with zipfile.ZipFile("/Users/mac/cloud/microisce/hosting/ansible.zip", 'r') as zip_ref:
        zip_ref.extractall(zip_folder)

    create_template(host=domain, owner=username, folder=zip_folder, user=fb_user, password=fb_pass, product=product,
                    worker_id=worker_id)

    r = ansible_runner.run(private_data_dir=zip_folder+"/ansible",
                           playbook=zip_folder+'/ansible/project/deploy_site.yml')

    return os.getcwd()


def create_template(host, owner, folder, user, password, product, worker_id):

    # https://acme-v02.api.letsencrypt.org/directory
    # https://acme-staging-v02.api.letsencrypt.org/directory
    """ssl_vars = {
        "acme_challenge_type": "http-01",
        "acme_directory": "https://acme-v02.api.letsencrypt.org/directory",
        "acme_version": 2,
        "remaining_days": 30,
        "acme_email": "info@micehost.com",
        "letsencrypt_dir": "/etc/letsencrypt",
        "letsencrypt_keys_dir": "/etc/letsencrypt/keys",
        "letsencrypt_csrs_dir": "/etc/letsencrypt/csrs",
        "letsencrypt_certs_dir": "/etc/letsencrypt/certs",
        "letsencrypt_account_key": "/etc/letsencrypt/account/account.key",
        "domain_name": host,
        "domain_slug": host.split(".")[0]+"_"+host.split(".")[1]
    } """

    product_table = {
        "hobbyist": 40960,
        "beginner": 409600
    }
    shared_vars = {
        "http_host": host,
        "http_port": 80,
        "site_owner": owner,
        "http_conf": host+".conf",


    }
    access_vars = {
        "http_host": host,
        "fb_user": user,
        "fb_pass": password,
    }
    vfs_vars = {
        "http_host": host,
        "inode": product_table[product]

    }
    callback_vars = {
        "runner_http_url": "http://127.0.0.1:8000/updateprogress/"+str(worker_id)+"/",
        "runner_http_headers": {"worker_id": "worker"}
    }
    path = folder+"/ansible/project/roles"
    try:
        with open(path+"/fileaccess/vars/main.yml", "w") as file_yml:
            documents = yaml.dump(access_vars, file_yml)

        with open(path+"/sharedhosting/vars/main.yml", "w") as share_yml:
            documents = yaml.dump(shared_vars, share_yml)
        with open(path+"/createvfs/vars/main.yml", "w") as vfs_yml:
            documents = yaml.dump(vfs_vars, vfs_yml)
        # with open(path+"/sslprovision/vars/main.yml", "w") as ssl_yml:
        #     documents = yaml.dump(ssl_vars, ssl_yml)
        with open(folder+"/ansible/env/settings", "w") as settings_yml:
            documents = yaml.dump(callback_vars, settings_yml)
        return
    except Exception:
        print(Exception.__traceback__)


@shared_task(name='my_first_task')
def my_first_task(duration):
    sleep(duration)

    return('first_task_done')


