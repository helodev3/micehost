from datetime import timezone

from django.db import models
from django.utils.html import mark_safe
from django.contrib.auth.models import User, UserManager


# Create your models here.


class Resident(models.Model):

    nom = models.CharField(max_length=10)
    prenom = models.CharField(max_length=10)
    pic = models.ImageField(null=True)
    age = models.IntegerField(default=10)

    def image_tag(self):

        lien = "https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=772661003309887&height=200&width=200&ext=1601819696&hash=AeQitdmwEGr7f8MB"
        return mark_safe('<img src="%s" width="200" height="200" />' % (lien))

    image_tag.short_description = 'Image'

    def __str__(self):
        return self.nom

    def get_prenom(self):
        return self.prenom


class Produit(models.Model):

    nom = models.CharField(max_length=20)
    description = models.TextField(blank=True)
    photo = models.ImageField(upload_to='medias')
    rank = models.IntegerField(default=0)
    link = models.CharField(default="", max_length=100)

    def __str__(self):
        return self.nom

class Plan(models.Model):

    produit_associe = models.ForeignKey(Produit, on_delete=models.CASCADE)
    nom = models.CharField(max_length=20)
    price = models.IntegerField()
    features = models.CharField(max_length=50, blank=True)
    RAM = models.CharField(max_length=20, blank=True)
    CPU = models.CharField(max_length=20, blank=True)
    CORE = models.CharField(max_length=20, blank=True)
    bandwidth = models.CharField(max_length=20, blank=True)
    rank = models.CharField(max_length=20, blank=True)
    access_or_cert = models.CharField(max_length=20, blank=True)
    disk = models.CharField(max_length=20, blank=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.nom+" "+self.produit_associe.nom


class Site(models.Model):

    client = models.ForeignKey("auth.User", on_delete=models.CASCADE)
    plan = models.ForeignKey("hosting.Hosting", on_delete=models.CASCADE)
    status = models.CharField(max_length=30, blank=True)
    link = models.TextField(default="")
    fb_user = models.CharField(default="", max_length=100)
    fb_password = models.CharField(default="", max_length=16)
    domain = models.CharField(max_length=253, default="")


class Hosting(models.Model):

    nom = models.CharField(max_length=30, blank=True, default="")
    slug = models.CharField(max_length=30, blank=True, default="")
    price = models.IntegerField()
    bandwidth = models.CharField(max_length=20, blank=True)
    disk = models.CharField(max_length=20, blank=True)
    file_manager = models.BooleanField()
    custom_domain = models.BooleanField()
    db = models.IntegerField()
    description = models.TextField(blank=True)

    def __str__(self):
        return self.nom

    def get_price(self):
        return str(self.price)


class VPS(models.Model):

    nom = models.CharField(max_length=20)
    price = models.IntegerField()
    disk = models.IntegerField()
    RAM = models.CharField(max_length=20, blank=True)
    CORE = models.IntegerField()
    bandwidth = models.CharField(max_length=20, blank=True)
    rank = models.CharField(max_length=20, blank=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.nom

