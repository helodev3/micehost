"""microisce URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from hosting import views

urlpatterns = [

    path('dashboard/', views.dashboard, name='dashboard'),
    path('updateprogress/<int:worker>/', views.update_progress, name='update_progress'),
    path('subscribe/hosting/<str:product>/', views.subscribe_wh, name='subscribe_wh'),
    path('products/wh/', views.products_wh, name='products_wh'),
    path('products/', views.products, name='products'),
    path('pricing/', views.pricing, name='pricing'),
    path('pricingwh/', views.pricing_wh, name='pricing_wh'),
    path('pricingvps/', views.pricing_vps, name='pricing_vps'),
    path('progress/', views.progress, name='progress'),
    path('accounts/logout/', views.logout, name='logout'),
    path('accounts/login/', views.login, name='login'),
    path('accounts/signup/', views.signup, name='signup'),
    path('accounts/password_reset/', views.password_reset, name='password_reset'),
    path('', views.index, name='index')]
