from concurrent.futures import TimeoutError
from google.cloud import pubsub_v1


def callback(message):
    print("Received message: {}".format(message))

    message.ack()
    return message

# TODO(developer)
# project_id = "your-project-id"
# subscription_id = "your-subscription-id"
# Number of seconds the subscriber should listen for messages


def lire():
    timeout = 15.0

    subscriber = pubsub_v1.SubscriberClient.from_service_account_file("../../sub.json")
    # The `subscription_path` method creates a fully qualified identifier
    # in the form `projects/{project_id}/subscriptions/{subscription_id}`
    #subscription_path = subscriber.subscription_path(project_id, subscription_id)

    subscription_path = "projects/microisce-web-service/subscriptions/bar"




    streaming_pull_future = subscriber.subscribe(subscription_path, callback=callback)
    #print("Listening for messages on {}..\n".format(subscription_path))

    # Wrap subscriber in a 'with' block to automatically call close() when done.
    with subscriber:
        try:
            # When `timeout` is not set, result() will block indefinitely,
            # unless an exception is encountered first.
            streaming_pull_future.result(timeout=timeout)
        except TimeoutError:
            streaming_pull_future.cancel()


