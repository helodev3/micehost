#   import os
from google.cloud import pubsub_v1
from celery import shared_task
from time import sleep


@shared_task()
def progres():

    publisher = pubsub_v1.PublisherClient.from_service_account_file("../../pub.json")
    """topic_name = 'projects/{project_id}/topics/{topic}'.format(
        project_id=os.getenv('GOOGLE_CLOUD_PROJECT'),
        topic='MY_TOPIC_NAME',  # Set this to something appropriate.
    )
    """
    topic_name = "projects/microisce-web-service/topics/bar"
    # publisher.create_topic(name=topic_name)
    for i in range(10, 100, 5):

        publisher.publish(topic_name, b'loading!', current=i)
        sleep(2)

