"""from google.cloud import datastore

client = datastore.Client.from_service_account_json('keys/storage.json')
bucket = datastore.Bucket("my-bucket-name")
bucket.location = "europe-west6"
bucket.storage_class = "COLDLINE"
# Pass that resource object to the client.
bucket = client.create_bucket(bucket)  # API request.
"""
from google.cloud import storage


def upload_file(id, source, dest):

    try:
        client = storage.Client.from_service_account_json("hosting/deployement/keys/storage.json")
        bucket = client.create_bucket(id, location="eu")
        source_filename = source
        destination_blob_name = dest
        blob = bucket.blob(destination_blob_name)
        blob.upload_from_filename(source_filename)
        blob.make_public()
        return blob.public_url

    except:
        return "0"
