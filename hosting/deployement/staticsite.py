
import logging
import boto3
from botocore.exceptions import ClientError
import json


def create_bucket(bucket_name, region=None):
    """Create an S3 bucket in a specified region

    If a region is not specified, the bucket is created in the S3 default
    region (us-east-1).

    :param bucket_name: Bucket to create
    :param region: String region to create bucket in, e.g., 'us-west-2'
    :return: True if bucket created, else False
    """

    website_configuration = {
        'ErrorDocument': {'Key': 'error.html'},
        'IndexDocument': {'Suffix': 'index.html'},
    }
    bucket_policy = {
        'Version': '2012-10-17',
        'Statement': [{
            'Sid': 'AddPerm',
            'Effect': 'Allow',
            'Principal': '*',
            'Action': ['s3:GetObject'],
            'Resource': "arn:aws:s3:::%s/*" % bucket_name
        }]
    }
    bucket_policy = json.dumps(bucket_policy)

    # Create bucket
    try:
        if region is None:
            s3_client = boto3.client('s3')
            s3_client.create_bucket(Bucket=bucket_name)
            s3_client.put_bucket_policy(Bucket=bucket_name, Policy=bucket_policy)
            s3_client.upload_file(Bucket=bucket_name, Filename="hosting/deployement/index.html", Key="index.html",ExtraArgs={'ContentType':'text/html'})
            s3_client.upload_file(Bucket=bucket_name, Filename="hosting/deployement/error.html", Key="error.html",ExtraArgs={'ContentType':'text/html'})
            s3_client.put_bucket_website(Bucket=bucket_name, WebsiteConfiguration=website_configuration)

            return s3_client
        else:
            s3_client = boto3.client('s3', region_name=region)
            location = {'LocationConstraint': region}
            s3_client.create_bucket(Bucket=bucket_name,
                                    CreateBucketConfiguration=location)
            s3_client.put_bucket_policy(Bucket=bucket_name, Policy=bucket_policy)
            s3_client.upload_file(Bucket=bucket_name, Filename="hosting/deployement/index.html", Key="index.html",ExtraArgs={'ContentType':'text/html'})
            s3_client.upload_file(Bucket=bucket_name, Filename="hosting/deployement/error.html", Key="error.html",ExtraArgs={'ContentType':'text/html'})
            s3_client.put_bucket_website(Bucket=bucket_name, WebsiteConfiguration=website_configuration)

            return s3_client
    except ClientError as e:
        logging.error(e)
        return False

