"""
from google.auth import app_engine

credentials = app_engine.Credentials()

#client = Client.from_service_account_json('keys/app-engine.json')
"""

from google.oauth2 import service_account
import googleapiclient.discovery


SCOPES = ['https://www.googleapis.com/auth/appengine.admin',
          "https://www.googleapis.com/auth/cloud-platform"]
SERVICE_ACCOUNT_FILE = 'keys/app-engine.json'

credentials = service_account.Credentials.from_service_account_file(
        SERVICE_ACCOUNT_FILE, scopes=SCOPES)

sqladmin = googleapiclient.discovery.build('appengine', 'v1', credentials=credentials)
#response = sqladmin.apps.regions.list(project='microisce-web-service').execute()
#print(dir(sqladmin.apps))

response = sqladmin.apps().create().execute()
