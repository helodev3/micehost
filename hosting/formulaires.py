from django import forms


class FormulaireSubscriptionWH(forms.Form):

    domain = forms.CharField(label="DOMAINE NAME", max_length=253, min_length=3, help_text="www.example.com")
    username = forms.CharField(label="FTP USERNAME", max_length=20)
    password = forms.CharField(label="FTP PASSWORD", widget=forms.PasswordInput())
    password_confirm = forms.CharField(label="CONFIRM FTP PASSWORD", widget=forms.PasswordInput())


class FormulaireFree(forms.Form):

    domain = forms.CharField(label="WEBSITE NAME", max_length=253, min_length=3)
    fichier = forms.FileField(label="ZIP FILE OF THE SITE")

    class Media:
        js = ['/static/js/progress_bar.js', '/static/admin/js/vendor/jquery/jquery.min.js',
              '/static/admin/js/jquery.init.js']


class FormulaireLogin(forms.Form):

    username = forms.CharField(label='Username', max_length=100)
    password = forms.CharField(widget=forms.PasswordInput())


class FormulaireSignUp(forms.Form):

    first_name = forms.CharField(label="Company Name", max_length=100)

    # last_name = forms.CharField(label="Last Name", max_length=100)
    email = forms.EmailField(label="Email")

#   username = forms.CharField(label='Username', max_length=100)
    password = forms.CharField(label="Password", widget=forms.PasswordInput())
    password_confirm = forms.CharField(label="Confirm password", widget=forms.PasswordInput())


class FormulaireSSL(forms.Form):

    company_name = forms.CharField(label="Company name", max_length=100)
