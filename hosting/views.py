import json
import dns.resolver
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .formulaires import *
from django.contrib.auth import authenticate, login as log_in, logout as log_out
from django.contrib import messages
from django.views.decorators.csrf import csrf_protect
from .models import *
from django.urls import reverse
from django.core.files.storage import FileSystemStorage
from .sub_test import lire
from .tasks import deployment
from django.views.decorators.csrf import csrf_exempt
# Create your views here.


def dashboard(request):

    if not request.user.is_authenticated:
        return redirect(reverse("login"))

    return render(request, "hosting/dashboard.html")


@csrf_exempt
def update_progress(request, worker=-1):

    if worker == -1:
        return HttpResponse("")
    worker_id = int(worker)
    data = json.loads(request.body)

    if "status" in data.keys():
        status = data["status"]
        if status == "successful":

            sub = Site.objects.get(id=worker_id)
            sub.status = "100"
            sub.save()
        return HttpResponse("")
    elif data["event"]:
        event = data["event"]
        if event == "runner_on_ok":
            task = data["event_data"]["task"]

            sub = Site.objects.get(id=worker_id)
            if task == "Sets Nginx conf file":

                sub.status = "10"
                sub.save()
                
            elif task == "Create a directory if it does not exist":

                sub.status = "20"
                sub.save()
                
            elif task == "Sets Homepage file":

                sub.status = "30"
                sub.save()

            elif task == "Enables new sites":

                sub.status = "40"
                sub.save()
                
            elif task == "Reload Server":

                sub.status = "50"
                sub.save()

            elif task == "Stop MiceFiles":

                sub.status = "60"
                sub.save()
                
            elif task == "Create User for filebrowser":

                sub.status = "70"
                sub.save()

            elif task == "Restart filebrowser":

                sub.status = "80"
                sub.save()
                
            elif task == "Reload Nginx":

                sub.status = "100"
                sub.save()

    return HttpResponse("")


@csrf_protect
def subscribe_wh(request, product):

    beg = "beginner"
    hob = "hobbyist"
    free = "free"
    if product == free:
        formulaire = FormulaireFree()
    else:
        formulaire = FormulaireSubscriptionWH()

    status = 200
    if not request.user.is_authenticated:

        return HttpResponseRedirect("/accounts/login")

    if request.POST and request.FILES['fichier']:

        domain = request.POST["domain"]
        full_domain = domain
        if product == free:
            message = ""
            error_flag = False
            if "." in domain or not domain.isalpha():
                message = "Website name should only contain characters from a to z"
                error_flag = True
            elif len(domain) < 3 or len(domain) > 15:
                message = "Website name length should be lesser than 15 and greater than 3"
                error_flag = True
            elif Site.objects.filter(domain=domain, plan__slug=product).count() != 0:
                message = "This website name is not available"
                error_flag = True
            if error_flag:
                messages.error(request, message)
                return render(request, "hosting/subscribe_wh.html", {"form": formulaire,
                                                                     "product": product})

            fichier = request.FILES['fichier']

            fs = FileSystemStorage()
            filename = fs.save(domain+".zip", fichier)

            if fs.size(domain+".zip") > 10000000:
                message = "Site file should not exceed 10MB"
                error_flag = True
            elif fs.size(domain+".zip") < 10:
                message = "There appears to be a problem with file, if the problem persists, please let us know"
                error_flag = True

            if error_flag:
                messages.error(request, message)
                return render(request, "hosting/subscribe_wh.html", {"form": formulaire,
                                                                     "product": product})

            uploaded_file_url = fs.url(filename)
            username = request.user.username
            client = User.objects.get(username=username)
            plan = Hosting.objects.get(slug=product)
            sub = Site()
            sub.client = client
            sub.plan = plan
            sub.domain = domain
            sub.status = "0"
            sub.save()
            return render(request, "hosting/subscribe_wh.html", {
                'uploaded_file_url': uploaded_file_url
            })

        if len(domain.split(".")) == 2:
            full_domain = "www."+domain
        try:

            ns_check = dns.resolver.resolve(full_domain)
            if ns_check.canonical_name.__str__() != "helodev.com.":
                messages.error(request, "Make sure to add the CNAME record from {0} to helodev.com".format(full_domain))
                return render(request, "hosting/subscribe_wh.html", {"form": formulaire,
                                                                     "product": product})
        except:
            messages.error(request, "Make sure to add the CNAME record from {0} to helodev.com".format(full_domain))
            return render(request, "hosting/subscribe_wh.html", {"form": formulaire,
                                                                 "product": product})

        if Site.objects.filter(domain=domain).exclude(plan__slug=free).count() != 0:
            messages.error(request, "There is another website on that domain")
            return render(request, "hosting/subscribe_wh.html", {"form": formulaire,
                                                                 "product": product})
        username = request.user.username

        ftp_user = request.POST["username"]
        ftp_pass = request.POST["password"]
        ftp_pass_confirm = request.POST["password_confirm"]

        if ftp_pass != ftp_pass_confirm:
            messages.error(request, "Password didn't match")
            return render(request, "hosting/subscribe_wh.html", {"form": formulaire,
                                                                 "product": product})

        client = User.objects.get(username=username)
        plan = Hosting.objects.get(slug=product)
        sub = Site()
        sub.client = client
        sub.plan = plan
        sub.fb_user = ftp_user
        sub.fb_password = ftp_pass
        sub.status = "0"
        sub.save()
        worker_id = sub.id
        deployment.delay(domain=domain, username=username, fb_user=ftp_user, fb_pass=ftp_pass, product=product,
                         worker_id=worker_id)
        return render(request, "hosting/subscribe_wh.html", {
            'uploaded_site': True
        })
    return render(request, "hosting/subscribe_wh.html", {"form": formulaire, "product": product},
                  status=status)


def pricing(request):
    context = dict()
    if request.user.is_authenticated:
        context["username"] = request.user.username

    return render(request, "hosting/pricing.html", context=context)


def pricing_wh(request):
    context = dict()
    if request.user.is_authenticated:
        context["username"] = request.user.username
    plans = Hosting.objects.all().order_by('price')
    context["plans"] = plans
    return render(request, "hosting/pricing_wh.html", context=context)


def pricing_vps(request):
    context = dict()
    if request.user.is_authenticated:
        context["username"] = request.user.username
    plans = VPS.objects.all().order_by('price')
    context["plans"] = plans
    return render(request, "hosting/pricing_vps.html", context=context)


@csrf_protect
def products_wh(request):
    status = 200
    if not request.user.is_authenticated:

        return HttpResponseRedirect("/accounts/login")
    if request.POST and request.FILES['fichier']:
        fichier = request.FILES['fichier']
        fs = FileSystemStorage()
        filename = fs.save(fichier.name, fichier)
        uploaded_file_url = fs.url(filename)
        return render(request, "hosting/wh_static/main.html", {
            'uploaded_file_url': uploaded_file_url
        })
    return render(request, "hosting/wh_static/main.html", {"form": FormulaireFree()}, status=status)


def products(request):
    context = dict()
    produits = Produit.objects.all().order_by('rank')
    context["produits"] = produits
    if request.user.is_authenticated:
        context["username"] = request.user.username
    return render(request, "hosting/products.html", context=context)


def index(request):

    #progres.delay()
    num_visits = request.session.get('num_visits', 0)
    request.session['num_visits'] = num_visits + 1
    context = {
        'num_visits': num_visits,
    }
    if request.user.is_authenticated:
        context["username"] = request.user.username
    # Render the HTML template index.html with the data in the context variable.
    return render(request, 'hosting/index.html', context=context)


@csrf_protect
def login(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/')
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            log_in(request, user)

            return HttpResponseRedirect('/')
        # Redirect to a success page.

        else:
            messages.error(request, "Invalid username or password")
            return render(request, 'hosting/login.html', {"form": FormulaireLogin()})
        # Return an 'invalid login' error message. ...
    else:
        return render(request, 'hosting/login.html', {"form": FormulaireLogin()})


def logout(request):
    if request.user.is_authenticated:
        log_out(request)
    return HttpResponseRedirect('/')


def password_reset(request):
    return HttpResponse("yeah")


@csrf_protect
def signup(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/')
    if request.POST:
        email = request.POST['email']
        password = request.POST['password']
        password_confirm = request.POST['password_confirm']
        first_name = request.POST['first_name']
        # user = authenticate(request, username=username, password=password)
        if password != password_confirm:
            messages.error(request, "Passwords don't match")
            return render(request, "hosting/signup.html", {"form": FormulaireSignUp()})

        user = User.objects.filter(email=email)

        if user.exists():
            messages.error(request, "An account with this email already exists")
            return render(request, "hosting/signup.html", {"form": FormulaireSignUp()})
        else :
            user = User.objects.create_user(first_name, email, password)

            user.first_name = first_name
            user.save()
            log_in(request, user)
            return HttpResponseRedirect('/')
        # Redirect to a success page.

    else:

        return render(request, "hosting/signup.html", {"form": FormulaireSignUp()})


def progress(request):

    etat = lire()
    return JsonResponse({"etat":etat}, safe=False)
