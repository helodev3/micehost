from django.contrib import admin
from .models import *

# Register your models here.


class ResidentAdmin(admin.ModelAdmin):
    list_display = ['nom', 'prenom']
    list_filter = ['nom']
    search_fields = ['nom', 'prenom']
    readonly_fields = ["image_tag"]


class HostingAdmin(admin.ModelAdmin):
    list_display = ["nom", "price"]
    search_fields = ["nom", "price"]


class VPSAdmin(admin.ModelAdmin):
    list_display = ["nom", "price", "disk", "RAM", "CORE"]


admin.site.register(Resident, ResidentAdmin)
admin.site.register(Produit)
admin.site.register(Plan)
admin.site.register(Site)
admin.site.register(Hosting, HostingAdmin)
admin.site.register(VPS, VPSAdmin)

